const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

// const priceOfObject=Object.entries(products).map((each)=>{
//     const eachProduct=each[1]
//     const price=Object.entries(eachProduct).map((eachPrice)=>{
//         return (eachPrice[1].price)
//     })
//     return price
// })

// const price=Object.entries(products)
// console.log((Object.entries(price[0][1].utensils)[0][1].spoons).price);
// console.log(priceOfObject);

// const priceOFItem=Object.values(products).reduce((group,current)=>{
//    return Object.values(current.utensils)
// },0)

// console.log(Object.keys(products));
// Q1. Find all the items with price more than $65.
const price=products.reduce((price,current)=>{
    Object.keys(current).map((each)=>{
        // console.log(each); 
        // console.log(current[each]);
        if(Array.isArray(current[each])){
            current[each].map((eachProductNested)=>{
                // console.log(eachProductNested);
                Object.keys(eachProductNested).map((itemPrice)=>{
                    if(Number(eachProductNested[itemPrice].price.split("$").join(""))>65){
                        price.push(itemPrice)
                    }
                })
            })
        }
        else{
            if(Number(current[each].price.split("$").join(""))>65){
                price.push(each)
            }
        }
    })
    return price
},[])
// console.log(price);

// Q2. Find all the items where quantity ordered is more than 1.

const quantity=products.reduce((quantity,current)=>{
    Object.keys(current).map((each)=>{
        // console.log(each); 
        // console.log(current[each]);
        if(Array.isArray(current[each])){
            current[each].map((eachProductNested)=>{
                Object.keys(eachProductNested).map((itemQuantity)=>{
                    if(Number(eachProductNested[itemQuantity].quantity)>1){
                        quantity.push(itemQuantity)
                    }
                })
            })
        }
        else{
            if(Number(current[each].quantity)>1){
                quantity.push(each)
            }
        }
    })
    return quantity
},[])
// console.log(quantity);

// Q.3 Get all items which are mentioned as fragile.

const fragileProduct=products.reduce((fragile,current)=>{
    Object.keys(current).map((each)=>{
        if(Array.isArray(current[each])){
            current[each].map((eachProductNested)=>{
                Object.keys(eachProductNested).map((fragileProduct)=>{
                    if(eachProductNested[fragileProduct].type=="fragile"){
                        fragile.push(fragileProduct)
                    }
                })
            })
        }
        else{
            if(current[each].type=="fragile"){
                fragile.push(each)
            }
        }
    })
    return fragile
},[])

console.log(fragileProduct);